#include <gtk/gtk.h>

/* When the standalone projector is run in full-screen on Linux, keyboard
 * controls no longer work. And even if window decorations are turned off on
 * the player normally, the menubar is still visible. Thus, to get a
 * "full screen" while still using keyboard controls, a small hack is needed to
 * prevent the creation of the kinds of GTK+ 2 panels that flashplayer tries
 * to make.

 * This is accomplished by using LD_PRELOAD to inject our own stub functions
 * over the ones that would normally be responsible for creating a panel, by
 * making our own small stub library take precedence over the original libgtk.

 * The widgets overridden below are the kinds of GTK widgets that flashplayer
 * tries to create, as verified using the GTK+ 2 version of 'gtkparasite'.

 * Since GTK+ 2 programs typically use g_free() to free allocated memory, and
 * g_free() does nothing if given a null pointer, it is likely safe in this
 * case to just have the stub functions return 'null' (zero) without ever
 * malloc'ing anything.
 */

/* return a null pointer from all the following functions (0) */
#define RET 0


#ifdef FULLSCREEN
#pragma message "If your window manager can be made to allow the user to disable window decorations on a per-window basis, you should comment out the FULLSCREEN_INJECT line in the Makefile. It's a very ugly hack for some common WM's like Metacity that don't give the user options."
/*
 * entering fullscreen and then pressing 'escape' should get you a fullscreen
 * window which you can use keyboard shortcuts in; it's a bad hack, but it
 * seems to work in more "normal" window managers that don't let me manually
 * turn off window borders/decorations like FVWM can (e.g., Metacity).
 */
void gtk_window_unfullscreen(GtkWindow *window)
{
  /* gtk_window_set_decorated() might not work on windows that are currently
     visible, so we try to sidestep that issue. */
  gtk_widget_hide((GtkWidget *)window);
  /* turn off decorations */
  gtk_window_set_decorated(window, FALSE);
  /* make window visible again, hopefully now decoration-free */
  gtk_widget_show((GtkWidget *)window);
  /* maximize decoration-less window to simulate fullscreen while keeping
  working keyboard input */
  gtk_window_maximize(window);
}
#endif /* FULLSCREENHACK */

GtkWidget* gtk_menu_bar_new(void)
{
  return RET;
}

GtkWidget* gtk_label_new (const gchar *str)
{
  return RET;
}

GtkWidget* gtk_entry_new (void)
{
  return RET;
}

GtkWidget * gtk_hbox_new (gboolean homogeneous, gint spacing)
{
  return RET;
}

